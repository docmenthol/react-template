import React from 'react'
import { Switch, Route, Link, NavLink, Redirect, useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'

// App Context
import ApplicationContext from './context'

// Pages
import Dashboard from './pages/Dashboard'
import Login from './pages/Login'
import Test from './pages/Test'

const useStyles = makeStyles(() => ({
  title: {
    marginRight: '1rem'
  },
  grow: {
    flexGrow: 1
  },
  headerLink: {
    textDecoration: 'none',
    color: 'inherit',
    margin: '0 0.5rem'
  },
  selected: {
    borderBottom: '1px solid white'
  },
  mainContainer: {
    paddingTop: '2em'
  }
}))

const App = () => {
  const classes = useStyles()
  const history = useHistory()
  const [ authenticated, setAuthenticated ] = React.useState(false)

  const handleLogin = () => {
    setAuthenticated(true)
    history.push('/')
  }

  const handleLogout = () => {
    setAuthenticated(false)
    history.push('/')
  }

  return (
    <>
      <CssBaseline />
      <AppBar position='relative'>
        <Toolbar>
          <Typography variant='h6' className={classes.title}>
            <Link to='/' className={classes.headerLink}>
              Application
            </Link>
          </Typography>


          <span className={classes.grow}>
            { authenticated &&
              <Typography variant='button'>
                <NavLink to='/test1' activeClassName={classes.selected} className={classes.headerLink}>
                  Test Section 1
                </NavLink>
                <NavLink to='/test2' activeClassName={classes.selected} className={classes.headerLink}>
                  Test Section 2
                </NavLink>
                <NavLink to='/test3' activeClassName={classes.selected} className={classes.headerLink}>
                  Test Section 3
                </NavLink>
              </Typography>
            }
          </span>

          { authenticated
            ? <Button onClick={handleLogout} color='inherit'>Logout</Button>
            : <Button component={Link} color='inherit' to='/login'>Login</Button>
          }

        </Toolbar>
      </AppBar>

      <ApplicationContext.Provider value={{ authenticated }}>
        <main className={classes.mainContainer}>
          <Switch>
            <Route path='/login'>
              <Login onLogin={handleLogin} />
            </Route>
            <Route path='/test1'>
              <Test path='1' />
            </Route>
            <Route path='/test2'>
              <Test path='2' />
            </Route>
            <Route path='/test3'>
              <Test path='3' />
            </Route>
            <Route exact path='/'>
              { authenticated ? <Dashboard /> : <Redirect to='/login' /> }
            </Route>
          </Switch>
        </main>
      </ApplicationContext.Provider>
    </>
  )
}

export default App
