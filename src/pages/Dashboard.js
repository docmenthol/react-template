import React from 'react'
import { Helmet } from 'react-helmet'
import {
  Container, Table, TableBody, TableCell, TableContainer,
  TableHead, TableRow, Paper
} from '@material-ui/core'

const courses = [
  { courseID: 'ENG1045', classTime: '9:15AM-10:50AM, M/W', instructor: 'Johnson', credits: 4 },
  { courseID: 'MAT1060', classTime: '11:15-12:00PM, M/W/F', instructor: 'Kim', credits: 4 },
  { courseID: 'MAT2001', classTime: '9:15AM-11:50AM, M/W', instructor: 'Williams', credits: 4 },
  { courseID: 'ART1033', classTime: '2:15PM-3:15PM, T/Th', instructor: 'Smith', credits: 1 },
]

const Dashboard = () =>
  <>
    <Helmet>
      <title>Dashboard</title>
    </Helmet>

    <Container>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableCell>Course ID</TableCell>
            <TableCell>Class Time</TableCell>
            <TableCell>Instructor</TableCell>
            <TableCell>Credits</TableCell>
          </TableHead>
          <TableBody>
            { courses.map((c, i) =>
              <TableRow key={`course-${i}`}>
                <TableCell>{ c.courseID }</TableCell>
                <TableCell>{ c.classTime }</TableCell>
                <TableCell>{ c.instructor }</TableCell>
                <TableCell align='right'>{ c.credits }</TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  </>

export default Dashboard
