import React from 'react'
import Container from '@material-ui/core/Container'

const Test = props =>
  <Container>
    <div>Test page #{props.path}.</div>
  </Container>


export default Test
