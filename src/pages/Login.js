import React from 'react'
import { Helmet } from 'react-helmet'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/paper'
import Container from '@material-ui/core/Container'
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Button from '@material-ui/core/Button'

import ApplicationContext from '../context'

const useStyles = makeStyles(() => ({
  loginPaper: {
    padding: '1em'
  },
  submit: {
    marginTop: '2em'
  }
}))

const Login = props => {
  const classes = useStyles()

  const [ username, setUsername ] = React.useState('')
  const [ password, setPassword ] = React.useState('')

  return (
    <>
      <Helmet>
        <title>Login</title>
      </Helmet>

      <Container maxWidth='sm'>
        <Paper className={classes.loginPaper}>
          <TextField
            variant='outlined'
            margin='normal'
            value={username}
            label='Username'
            onChange={e => setUsername(e.target.value)}
            fullWidth
          />

          <TextField
            variant='outlined'
            margin='normal'
            value={password}
            label='Password'
            type='password'
            onChange={e => setPassword(e.target.value)}
            fullWidth
          />

          <FormControlLabel
            control={<Checkbox value='remember' color='primary' />}
            label='Remember Me'
          />

          <ApplicationContext.Consumer>
            { context =>
              <Button
                fullWidth
                variant='contained'
                color='primary'
                className={classes.submit}
                onClick={props.onLogin}
              >
                Login
              </Button>
            }
          </ApplicationContext.Consumer>
        </Paper>
      </Container>
    </>
  )
}

export default Login
